import photos from "../img/photo.js"


document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems, {});
});

//add photos
photos.forEach( photo => {
    let image = document.createElement("img")
    image.src = `./assets/img/${photo}`
    image.classList.add("materialboxed")
    document.getElementById("placePhotos").appendChild(image)
}) 
