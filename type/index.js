const hasNumber = (str) => { /[0-9]/.test(str) }
const hasLower = (str) => { /[a-z]/.test(str) }
const hasUpper = (str) => { /[A-Z]/.test(str) }
const hasSpecial = (str) => { /[!@#$%^&*()]/.test(str) }
const isLength = (str, num) => { (str.length >= num) }

const meetsReqs = (str) => {[ hasNumber(str) , hasLower(str) , hasUpper(str) , hasSpecial(str) , isLength(str, 4) ] }

const shortReqs = (str) => ({
	number: /[0-9]/.test(str),
	lower: /[a-z]/.test(str),
	upper: /[A-Z]/.test(str),
	special: /[!@#$%^&*()]/.test(str),
	size: (str.length >= 4)
})

const place = function (str) {
 	document.getElementById("placeInput").innerHTML = `<input id="readInput" type="text"   placeholder="" class="validate" > <label for="readInput">Type Away!</label>`
}

function handleInput(e) {
       let str = document.getElementById("readInput").value
       var check = shortReqs(str)
       for (let [key, value] of Object.entries(check)) {
         value ? document.getElementById(key).classList.add(value) : document.getElementById(key).classList.remove("true") 
        }
}

function checkInput(e) {
    let input = document.getElementById("readInput").value
    let test = true || "some shit"
    let num = hasNumber(input) || `\nA number`
    let up = hasUpper(input) || `\nAn Uppercase letter`
    let low = hasLower(input) || `\nA Lowercase letter` 
    let special = hasSpecial(input) || `\nA special character "!@#$%^&*()"`
//    let len = input.length() > 4 || "A length of 4 characters or more" 
    alert(`${test} \n You wrote: \n ${input} \nIt's still missing:\n ${num}${up}${low}${special}`)
}

const start = function () {
	place("placeInput");
	let input = document.getElementById('readInput');
	input.addEventListener('input', handleInput)
} 

