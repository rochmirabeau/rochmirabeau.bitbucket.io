let globalIngredients = new Map
globalIngredients.set("lock" , false)
globalIngredients.set("locker" , "")

function sanitize(str) {
	return str.replace(/ /g, "+")
}

async function getImage() {
  const response = await fetch("./assets/image.jpg");
  const blob = await response.blob();
  document.getElementById("test").src = URL.createObjectURL(blob);
}

async function getRandomDrink() {
  const response = await fetch(
    "https://www.thecocktaildb.com/api/json/v1/1/random.php",
    { method: "get" }
  );
  const json = await response.json();
  const drink = await json.drinks[0];

  document.getElementById("drinkImg").src = drink.strDrinkThumb;
  document.getElementById("drinkDes").innerHTML = drink.strInstructions;
  document.getElementById("drinkNam").innerHTML = drink.strDrink;
  document.getElementById("drinkAlc").innerHTML = drink.strAlcoholic;

  // reduce drinks to ingredients
  const ingredients = Object.entries(drink)
    .reduce((arr, [key, value]) => {
      if (key.includes("Ingredient")) { 
		  arr.push(value);
	  }
      return arr;
    }, [])
    .filter((x) => x);

  // get descriptions for ingredients, store them in globalIngredients
  ingredients.forEach( async (ing) => {
	let description = await getDescription(ing)  
	globalIngredients[ ing ]  = description
  }) 

  const listIngredients = ingredients
    .map(
      (x) => { 
		const name = x.split(" ").join("+").toString()
        return `<li class=ingredient id=${name} onmouseover=updateDescription('${name}') onClick=lockDescription('${name}')>${x}</li>`
	  })
    .join("");

  document.getElementById(
    "drinkIng"
  ).innerHTML = `<ul> ${listIngredients} </ul> `;
  
  return drink;
}
getRandomDrink();

function clickMe(str) {
  const ingredient = document.getElementById(str).innerHTML;
  document.getElementById("getIngredientInfo").innerHTML = ingredient;
}

function updateButton(str) {
  const ingredient = document.getElementById(str).innerHTML;
  document.getElementById("newIngredient").innerHTML = ingredient;
}

async function getNewDrink(str) {
  const response = await fetch(
    `https://www.thecocktaildb.com/api/json/v1/1/search.php?i=${str}`,
    { method: "get" }
  );
	const json = await response.json()
	const ingredient = json.ingredients[0]
	const name = ingredient.strIngredient
}

async function getDescription(str) {
  const response = await fetch(
    `https://www.thecocktaildb.com/api/json/v1/1/search.php?i=${str}`,
    { method: "get" }
  );
  const json = await response.json();
  const blurb = await json.ingredients[0].strDescription
  return blurb || "We didn't find any info on this ingredient"
}

async function getInfo(str) {
  const info = document.getElementById("ingredientInfo")
  const ingredient = document.getElementById(str).innerHTML.trim();
  (ingredient !== "Pick an Ingredient") ? info.innerHTML = "...Searching..." : info.innerHTML = "Pick an ingredient first."
  const response = await fetch(
    `https://www.thecocktaildb.com/api/json/v1/1/search.php?i=${ingredient}`,
    { method: "get" }
  );
  let json = await response.json();
  let blurb = await json.ingredients[0].strDescription
  if (blurb) {
  //split p into paragraphs
    blurb = blurb.split(`\r`)
     .map(line => `${line} <br>`)
     .join("")
  }
  blurb ? info.innerHTML = blurb : info.innerHTML = "We didn't find any info on that ingredient";
  return blurb
}

let hoveredIngredients = document.querySelector("li.ingredient")

async function updateDescription(str, lock) {
	const name = str.split("+").join(" ")
	const info = document.getElementById("ingredientInfo")
	if (!globalIngredients.get("lock")) info.innerHTML = globalIngredients[name]
}

// prevent css from changing, prevent description from changing
function lockDescription(str) {
	const name = str
	const locker = globalIngredients.get("locker")
	const ingredients = document.getElementsByClassName("ingredient")

	console.log(locker)
	console.log(str)
	//clear remove classes locked, locker restoring original functionality
	if (locker === str) {
		//if locker is the same then clear everything
		let main = document.getElementById(name)
		main.classList.remove('locker')
		main.classList.remove('mainIngredient')
		let ingredients = document.getElementsByClassName("locked")
		console.log(ingredients)
		globalIngredients.set("lock" , false)
		return
	}
	else {
		//else make a new locker
		let main = document.getElementById(str)
		// remove the locker
		let oldLocker = document.getElementById(locker)
		if (oldLocker) {
			oldLocker.classList.remove("locker")
			oldLocker.classList.remove("mainIngredient")
		}
		main.classList.add("locker")
		main.classList.add("mainIngredient")
		let ingredients = document.getElementsByClassName("ingredients")
		globalIngredients.set("lock" , true)
		globalIngredients.set("locker" , name)
	}
}
