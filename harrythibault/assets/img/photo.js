const dadPhotos = [
"folder1/Thibault100_0260.JPG",
"folder1/Thibault100_0306.JPG",
"folder1/Thibault100_0307.JPG",
"folder1/Thibault100_0310.JPG",
"folder1/Thibault100_0312.JPG",
"folder1/Thibault100_0324.JPG",
"folder1/Thibault100_0328.JPG",
"folder1/Thibault100_0330.JPG",
"folder1/Thibault100_0336.JPG",
"folder1/Thibault100_0340.JPG",
"folder2/Thibault100_0280.JPG",
"folder2/Thibault100_0295.JPG",
"folder2/Thibault100_0302.JPG",
"folder2/Thibault100_0303.JPG",
"folder2/Thibault100_0315.JPG",
"folder2/Thibault100_0319.JPG",
"folder2/Thibault100_0320.JPG",
"folder2/Thibault100_0347.JPG",
"folder2/Thibault100_0348.JPG",
"folder3/Thibault100_0263.JPG",
"folder3/Thibault100_0271.JPG",
"folder3/Thibault100_0272.JPG",
"folder3/Thibault100_0275.JPG",
"folder3/Thibault100_0294.JPG",
"folder3/Thibault100_0301.JPG",
"folder3/Thibault100_0308.JPG",
"folder3/Thibault100_0338.JPG",
]

const photos = [
    "folder5/IMG-20200703-WA0000.jpg",
    "folder5/IMG-20210201-WA0002.jpg",
    "folder5/IMG-20210203-WA0004.jpg",
    "folder5/IMG-20210203-WA0008.jpg",
    "folder1/Thibault100_0260.JPG",
    "folder1/Thibault100_0306.JPG",
    "folder1/Thibault100_0310.JPG",
    "folder1/Thibault100_0324.JPG",
    "folder1/Thibault100_0328.JPG",
    "folder1/Thibault100_0336.JPG",
    "folder1/Thibault100_0340.JPG",
    "folder2/Thibault100_0280.JPG",
    "folder2/Thibault100_0295.JPG",
    "folder2/Thibault100_0302.JPG",
    "folder2/Thibault100_0315.JPG",
    "folder2/Thibault100_0320.JPG",
    "folder2/Thibault100_0347.JPG",
    "folder3/Thibault100_0263.JPG",
    "folder3/Thibault100_0272.JPG",
    "folder3/Thibault100_0294.JPG",
    "folder3/Thibault100_0301.JPG",
    "folder3/Thibault100_0338.JPG",
    "folder4/test001.jpg",
    "folder4/test002.jpg",
    "folder4/test003.jpg",
    "folder4/test004.jpg",
    "folder4/test005.jpg",
    "folder4/test006.jpg",
]

export default photos
